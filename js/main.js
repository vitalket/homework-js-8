/* 
   1. Document Object Model (DOM) - дерево або структура html сторінки, з якою можна взаємодіяти та змінювати, наприклад, через JS 

   2. Встановлення innerHTML змінює не тільки текстовий вміст, але й HTML-структуру всередині елемента
      Встановлення innerText змінює тільки текстовий вміст елемента, і будь-який попередній HTML вміст буде замінений на новий текст.

   3. За допомогою getElementById(), getElementsByTagName(), getElementsByClassName(), querySelector(), querySelectorAll()
      Кращий спосіб - querySelector(), querySelectorAll()

 */

const paragraph = document.querySelectorAll("p");
const optionList = document.querySelector("#optionsList");
const optionsListText = document.querySelector("#testParagraph");
const mainHeader = document.querySelector(".main-header");
const sectionTitle = document.querySelectorAll(".section-title");

paragraph.forEach(elem => {
   elem.style.cssText = `
      background-color: #ff0000;
      opacity: 1;
   `;
});

console.log(optionList);
console.log(optionList.parentElement);
console.log(optionList.childNodes);

optionsListText.textContent = "This is a paragraph";

const mainHeaderChildren = [...mainHeader.querySelectorAll('*')];
console.log(mainHeaderChildren);

mainHeaderChildren.forEach(elem => {
   elem.classList.add("nav-item")
});

sectionTitle.forEach(elem => {
   elem.classList.remove("section-title");
});


















